<?php

use Symfony\Component\VarDumper\VarDumper;

_auth();
$ui->assign('_application_menu', 'kurir');
$ui->assign('_title', 'Kurir'.'- '. $config['CompanyName']);
$ui->assign('_st', 'Kurir');
$action = $routes['1'];
$user = User::_info();
$ui->assign('user', $user);

Event::trigger('orders');

switch ($action) {
    case 'add':
        $d = ORM::for_table('crm_accounts')->order_by_desc('id')->find_many();
        // $provinsi = ORM::for_table('sys_wilayah')->whereRaw('CHAR_LENGTH(kode) = 5')->find_many();
        $provinsi = ORM::for_table('origin_jne')->select('KAB_KOTA')->distinct()->find_many();
        // $provinsi = array_unique($provinsi[0]['KAB_KOTA']);
        // var_dump($provinsi);
        // die();
        $ui->assign('pe',$provinsi);
        // $phpvariable = "<scrip>document.write(localStorage.getItem('specintruction'))</scrip>";
        // $getdataprov = ORM::for_table('sys_wilayah')->where_like('kode',$getdataprov.'.%')->whereRaw('CHAR_LENGTH(kode) = 5')->find_many();
        // var_dump($phpvariable);
        // die();
        $ui->assign('xheader', Asset::css(array('modal','s2/css/select2.min')));
        $mode_js = Asset::js(array('s2/js/select2.min','kurir/add1','kurir/add2'));
        $ui->assign('xfooter',$mode_js);
        $ui->assign('xjq','
        $("#pengirim").select2({
            theme: "bootstrap"
            });
        $("#penerima").select2({
            theme: "bootstrap"
            });
        // console.log(testing);
        ');
        $ui->assign('d',$d);
        $ui->assign('p',$d);
        // session_destroy();
        $ui->display('add-kurir.tpl');
        break;
    case 'add2':
        // Event::trigger('kurir/add/');
        // Event::trigger('kurir/add/_on_start');
        // Event::trigger('kurir/add/_on_display');
        $penerima = _post('penerima');
        $pengirim = _post('pengirim');
        $weight = _post('weight');
        // JNE
        $origin_jne_pengirim = ORM::for_table('origin_jne')->where('KAB_KOTA',$pengirim)->find_one();
        $origin_jne_penerima = ORM::for_table('origin_jne')->where('KAB_KOTA',$penerima)->where('KECAMATAN','Bandung')->find_one();
        $jne_pengirim_kode = $origin_jne_pengirim['KODE'];
        $jne_penerima_kode = $origin_jne_penerima['KODE'];

        // SiCepat
        // $origin_Sicepat_pengirim = ORM::for_table('origin_sicepat')->where_like('city','%'.$pengirim.'%')->find_one();
        // $origin_Sicepat_penerima = ORM::for_table('origin_sicepat')->where_like('city','%'.$pengirim.'%')->find_one();
        // $Sicepat_pengirim_kode = substr($origin_Sicepat_pengirim['destination_code'], 0,3); ;
        // $Sicepat_penerima_kode = $origin_Sicepat_penerima['destination_code'];

        // $jne = Jne::send('tarif',$jne_pengirim_kode,$jne_penerima_kode, $weight);
        $jne = Jne::send('tarif',$weight,$jne_pengirim_kode,$jne_penerima_kode);
        // var_dump($jne);
        // echo $weight,$jne_pengirim_kode,$jne_penerima_kode;
        // die();
        // $sicepat = Sicepat::send('tariff',$weight,$Sicepat_pengirim_kode,$Sicepat_penerima_kode);
        // $tarif = $test['price'];
        // print_r($origin_jne['KODE']);
        // var_dump($sicepat);
        // die();
        $ui->assign('pos',$jne);
        // $ui->assign('cepat',$sicepat);
        // session_destroy();

        $ui->assign('xheader', Asset::css(array('modal','s2/css/select2.min')));
        $mode_js = Asset::js(array('s2/js/select2.min','kurir/add2'));
        $ui->assign('xfooter',$mode_js);
        $ui->display('add-kurir2.tpl');
        break;
    case 'add3':
        $d = ORM::for_table('crm_accounts')->order_by_desc('id')->find_many();
        $provinsi = ORM::for_table('sys_wilayah')->whereRaw('CHAR_LENGTH(kode) = 2')->find_many();
        // var_dump($provinsi);
        // die();
        $ui->assign('on', $provinsi);
        $ui->assign('xheader', Asset::css(array('modal','s2/css/select2.min')));
        $mode_js = Asset::js(array('s2/js/select2.min','kurir/add3'));
        $ui->assign('xfooter',$mode_js);
        $ui->assign('xjq','
        $("#city").select2({
            theme: "bootstrap"
            });
        $("#district").select2({
            theme: "bootstrap"
            });
        $("#state").select2({
            theme: "bootstrap"
            });
        $("#village").select2({
            theme: "bootstrap"
            });
        $("#state").change(function(){
            let state = $("#state").val()
            $.get("'.U.'ajax.wilayah/provid/" + state , function( data ) {
                $( "#city" ).html( data );
            })
        });
        $("#city").change(function(){
            let city = $("#city").val()
            $.get("'.U.'ajax.wilayah/kecid/" + city , function( data ) {
                $( "#district" ).html( data );
            })
        });
        $("#district").change(function(){
            let district = $("#district").val()
            $.get("'.U.'ajax.wilayah/desid/" + district , function( data ) {
                $( "#village" ).html( data );
            })
        });
        ');
        $ui->assign('d',$d);
        $ui->display('add-kurir3.tpl');
        break;
    case 'add4':
        // $mode_js = Asset::js(array('s2/js/select2.min','kurir/add'));
        $d = ORM::for_table('crm_accounts')->order_by_desc('id')->find_many();
        $provinsi = ORM::for_table('sys_wilayah')->whereRaw('CHAR_LENGTH(kode) = 2')->find_many();
        // var_dump($provinsi);
        // die();
        $ui->assign('on', $provinsi);
        $ui->assign('xheader', Asset::css(array('modal','s2/css/select2.min')));
        $mode_js = Asset::js(array('s2/js/select2.min','kurir/add4'));
        $ui->assign('xfooter',$mode_js);
        $ui->assign('xjq','
        $("#member").change(function(){
            let member = $("#member").val()
            $.get("'.U.'ajax.kurir/pengirim/" + member , function( data ) {
                $( "#nohp" ).html(data);
                // alert(member);
            })
        });
        $("#city").select2({
            theme: "bootstrap"
            });
        $("#district").select2({
            theme: "bootstrap"
            });
        $("#state").select2({
            theme: "bootstrap"
            });
        $("#village").select2({
            theme: "bootstrap"
            });
        $("#state").change(function(){
            let state = $("#state").val()
            $.get("'.U.'ajax.wilayah/provid/" + state , function( data ) {
                $( "#city" ).html( data );
            })
        });
        $("#city").change(function(){
            let city = $("#city").val()
            $.get("'.U.'ajax.wilayah/kecid/" + city , function( data ) {
                $( "#district" ).html( data );
            })
        });
        $("#district").change(function(){
            let district = $("#district").val()
            $.get("'.U.'ajax.wilayah/desid/" + district , function( data ) {
                $( "#village" ).html( data );
            })
        });
        // $("#member").on("change", function() {
        //     var comboid = $(this).val();

        //     $.ajax({
        //         url: "'.U.'aja.kurir/pengirim/"
        //     })
        // })
        ');
        $ui->assign('d',$d);
        $ui->display('add-kurir4.tpl');

        break; 

        case 'add-kurir':

            Event::trigger('contacts/add-kurir/');
    
            Event::trigger('contacts/add-kurir/_on_start');
    
            $account = _post('account');
    
    
            //  $company = _post('company');
    
            $company_id = _post('cid');
    
            $company = '';
            $cid = 0;
    
            if($company_id != '' && $company_id != '0'){
                $company_db = db_find_one('sys_companies',$company_id);
    
                if($company_db){
                    $company = $company_db->company_name;
                    $cid = $company_id;
                }
    
    
            }
    
    
    
    
            // $email = _post('email');
            // $phone = _post('phone');
            // $currency = _post('currency');
    
            // //          
    
            // $address = _post('address');
            // $city = _post('city');
            // $state = _post('state');
            // $zip = _post('zip');
            // $district = _post('district');
            // $village = _post('village');
            // $country = _post('country');

            $pengirim_kota = _post('pengirim_kota');
            $penerima_kota = _post('penerima_kota');
            $spec_intruction = _post('spec_intruction');
            $weight = _post('weight');
            $vweight = _post('vweight');
            $content = _post('content');
            $pieces = _post('pieces');
            $insurance = _post('insurance');
            $transport = _post('transport');
            $layanan = _post('layanan');
            $nama_penerima = _post('nama_penerima');
            $nohp_penerima = _post('nohp_penerima');
            $email_penerima = _post('email_penerima');
            $alamat_penerima = _post('alamat_penerima');
            $provinsi_penerima = _post('provinsi_penerima');
            $kota_penerima = _post('kota_penerima');
            $kecamatan_penerima = _post('kecamatan_penerima');
            $desa_penerima = _post('desa_penerima');
            $nama_pengirim = _post('nama_pengirim');
            $nohp_pengirim = _post('nohp_pengirim');
            $email_pengirim = _post('email_pengirim');
            $alamat_pengirim = _post('alamat_pengirim');
            $provinsi_pengirim = _post('provinsi_pengirim');
            $kota_pengirim = _post('kota_pengirim');
            $kecamatan_pengirim = _post('kecamatan_pengirim');
            $desa_pengirim = _post('desa_pengirim');
            $date=_post('date');

            $msg = '';
    
        //check if tag is already exisit
        
        
        
                // if($account == ''){
                //     $msg .= $_L['Account Name is required'].' <br>';
                // }
        
        //check account is already exist
        //        $chk = ORM::for_table('crm_accounts')->where('account',$account)->find_one();
        //        if($chk){
        //            $msg .= 'Account already exist <br>';
        //        }
    
            // if($email != ''){
            //     if(Validator::Email($email) == false){
            //         $msg .= $_L['Invalid Email'].' <br>';
            //     }
            //     $f = ORM::for_table('crm_accounts')->where('email',$email)->find_one();
    
            //     if($f){
            //         $msg .= $_L['Email already exist'].' <br>';
            //     }
            // }
    
    
            // if($phone != ''){
    
            //     $f = ORM::for_table('crm_accounts')->where('phone',$phone)->find_one();
    
            //     if($f){
            //         $msg .= $_L['Phone number already exist'].' <br>';
            //     }
            // }
    
    
            $gid = _post('group');
    
            // if($gid != ''){
            //     $g = db_find_one('crm_groups',$gid);
            //     $gname = $g['gname'];
            // }
            // else{
            //     $gid = 0;
            //     $gname = '';
            // }
    
            // $u_password = '';
    
    
            // if($password != ''){
    
            //     if(!Validator::Length($password,15,5)){
            //         $msg .= 'Password should be between 6 to 15 characters'. '<br>';
    
            //     }
    
            //     if($password != $cpassword){
            //         $msg .= 'Passwords does not match'. '<br>';
            //     }
    
    
            //     $u_password = $password;
            //     $password = Password::_crypt($password);
    
    
            // }
    
    
    
    
    
    
            if($msg == ''){
    
                Tags::save($tags,'Contacts');
    
                $data = array();
    
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
    
                $d = ORM::for_table('crm_accounts')->create();
    
                $d->account = $account;
                $d->email = $email;
                $d->phone = $phone;
                $d->address = $address;
                $d->city = $city;
                $d->zip = $zip;
                $d->state = $state;
                $d->country = $country;
                $d->district = $district;
                $d->village = $village;
                $d->tags = Arr::arr_to_str($tags);
    
                //others
                $d->fname = '';
                $d->lname = '';
                $d->company = $company;
                $d->jobtitle = '';
                $d->cid = $cid;
                $d->o = $user->id;
                $d->balance = '0.00';
                $d->status = 'Active';
                $d->notes = '';
                $d->password = $password;
                $d->token = '';
                $d->ts = '';
                $d->img = '';
                $d->web = '';
                $d->facebook = '';
                $d->google = '';
                $d->linkedin = '';
    
                // v 4.2
    
                $d->gname = $gname;
                $d->gid = $gid;
    
                // build 4550
    
                $d->currency = $currency;
    
                //
    
                $d->created_at = $data['created_at'];
    
                //
                $d->save();
                $cid = $d->id();
                _log($_L['New Contact Added'].' '.$account.' [CID: '.$cid.']','Admin',$user['id']);
    
                //now add custom fields
                $fs = ORM::for_table('crm_customfields')->where('ctype','crm')->order_by_asc('id')->find_many();
                foreach($fs as $f){
                    $fvalue = _post('cf'.$f['id']);
                    $fc = ORM::for_table('crm_customfieldsvalues')->create();
                    $fc->fieldid = $f['id'];
                    $fc->relid = $cid;
                    $fc->fvalue = $fvalue;
                    $fc->save();
                }
                //
    
                Event::trigger('contacts/add-post/_on_finished');
    
                // send welcome email if needed
    
                $send_client_signup_email = _post('send_client_signup_email');
    
    
                if(($email != '') && ($send_client_signup_email == 'Yes') && ($u_password != '')){
    
                    $email_data = array();
                    $email_data['account'] = $account;
                    $email_data['company'] = $company;
                    $email_data['password'] = $u_password;
                    $email_data['email'] = $email;
    
                    $send_email = Ib_Email::send_client_welcome_email($email_data);
    
    
    
                }
    
    
    
                echo $cid;
    
    
    
            }
            else{
                echo $msg;
            }
            break;
    case 'render-people':

        Event::trigger('kurir/render-people/');

        $member = _post('member');
        $d = ORM::for_table('crm_accounts')->find_one($cid);
        $address = $d['address'];
        $city = $d['city'];
        $state = $d['state'];
        $zip = $d['zip'];
        $country = $d['country'];
        $phone = $d['phone'];
        echo "$phone";
        break;

    case 'set-awbill':
        $weight = _post('weight');
        // $pengirim = _post('pengirim');
        // $penerima = _post('penerima');
        $exampleRadios =  _post('exampleRadios');
        $_SESSION['weight'] = $weight;
        // $_SESSION['pengirim'] = $pengirim;
        // $_SESSION['penerima'] = $penerima;
        $_SESSION['exampleRadios'] = $exampleRadios;

        echo 'success';

    case 'awbill':
        // $ui->assign('xheader', Asset::css(array('modal','s2/css/select2.min')));
        // $mode_js = Asset::js(array('s2/js/select2.min','kurir/add4'));
        // $ui->assign('xfooter',$mode_js);
        $orderid = mt_rand();
        // mt_rand(1000000, 9999999)
        // var_dump($orderid);
        // die();
        $weight = $_SESSION['weight'];
        // $pengirim = $_SESSION['pengirim'];
        // $penerima = $_SESSION['penerima'];
        $layanan = $_SESSION['exampleRadios'];
        // session_destroy(); 
        // var_dump($layanan);
        // echo $weight,  $layanan, $orderid;
        // die();
        // var_dump($weight,  $layanan, $orderid);
        // die();
        $jne = Jne::send('awb',$weight,'','',$orderid,$layanan);
        $ui->assign('d',$jne);
        $ui->assign('pe',$originku1);
        $ui->display('kurir-cnote.tpl');
        // if ($layanan === 'JTR' && $layanan === 'JTR250' && $layanan === 'JTR250' && $layanan === 'JTR<150' && $layanan === 'JTR>250' && $layanan === 'OKE' && $layanan === 'REG' && $layanan === 'SPS' &&$layanan ==='YES') {
        //     $jne = Jne::send('awb',$weight,$orderid,$layanan);
        //     $ui->assign('d',$jne);
        //     $ui->assign('pe',$originku1);
        //     $ui->display('kurir-cnote.tpl');
        //     // unset($_SESSION['exampleRadios']);
        //     // session_destroy();
        // } elseif ($layanan === 'BEST' && $layanan === 'GOKIL' && $layanan === 'REG' && $layanan === 'SDS' && $layanan === 'SIUNT') {
        //     $ui->display('kurir-cnote-sicepat.tpl');
        // }
        // var_dump($layanan);
        // die();
        // $originku1 = ORM::for_table('sys_wilayah')->where_like('kode','%'.$pengirim.'%')->whereRaw('CHAR_LENGTH(kode) = 5')->find_many();
        // $cutorigin = substr($originku1[0]['nama'], 5,15);

        // print_r($cutorigin);
        // die();

        // JNE
        // $origin_jne_pengirim = ORM::for_table('origin_jne')->where('KAB_KOTA',$pengirim)->find_one();
        // $origin_jne_penerima = ORM::for_table('origin_jne')->where('KAB_KOTA',$penerima)->where('KECAMATAN','Bandung')->find_one();
        // $jne_pengirim_kode = $origin_jne_pengirim['KODE'];
        // $jne_penerima_kode = $origin_jne_penerima['KODE'];

        // // SiCepat
        // $origin_Sicepat_pengirim = ORM::for_table('origin_sicepat')->where_like('city','%'.$pengirim.'%')->find_one();
        // $origin_Sicepat_penerima = ORM::for_table('origin_sicepat')->where_like('city','%'.$pengirim.'%')->find_one();
        // $Sicepat_pengirim_kode = substr($origin_Sicepat_pengirim['destination_code'], 0,3); ;
        // $Sicepat_penerima_kode = $origin_Sicepat_penerima['destination_code'];
        // foreach ($originku1 as $a) {
        //     echo $a['kode'];
        // }
        // var_dump($weight);
        // die();
        // $jne = Jne::send('awb',$weight,$orderid,$layanan);
        break;
    default:
        echo 'action not defined';
        break;
}