<?php
class Sicepat
{
    public static  function send($category,$value = 0, $pengirim = 0, $penerima = 0)
    {
        switch ($category) {
            case 'origin':
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://apitrek.sicepat.com/customer/origin',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'api-key: 1c9dd8bd7b4bff08de9bacdfc7b4bace'
                ),
                ));

                $response = json_decode(curl_exec($curl), 1);

                curl_close($curl);
                return $response['sicepat']['results'];
                break;
            case 'destination':
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://apitrek.sicepat.com/customer/destination',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'api-key: 1c9dd8bd7b4bff08de9bacdfc7b4bace'
                ),
                ));

                $response = json_decode(curl_exec($curl), 1);

                curl_close($curl);
                return $response['sicepat']['results'];
                break;
            case 'tariff':
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://apitrek.sicepat.com/customer/tariff?weight='.$value.'&origin='.$pengirim.'&destination='.$penerima.'',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'api-key: 1c9dd8bd7b4bff08de9bacdfc7b4bace'
                ),
                ));

                $response = json_decode(curl_exec($curl), 1);

                curl_close($curl);
                return $response['sicepat']['results'];
                break;
            default:
                # code...
                break;
        }
    }
}
