<?php
class Jne
{
    public static function send($category, $weight = 0, $pengirim = 0, $penerima = 0, $orderid = 0, $layanan = 0)
    {
        switch ($category) {
            case 'tarif':
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://apiv2.jne.co.id:10102/tracing/api/pricedev',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => 'username=TESTAPI&api_key=25c898a9faea1a100859ecd9ef674548&from='.$pengirim.'&thru='.$penerima.'&weight='.$weight.'',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded'
                ),
                ));

                $response = json_decode(curl_exec($curl), 1);

                curl_close($curl);
                return $response['price'];
                break;
            case 'awb':
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://apiv2.jne.co.id:10102/tracing/api/generatecnote',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => 'username=TESTAPI&api_key=25c898a9faea1a100859ecd9ef674548&OLSHOP_BRANCH=CGK000&OLSHOP_CUST=10950700&OLSHOP_ORDERID='.$orderid.'&OLSHOP_SHIPPER_NAME=ALI&OLSHOP_SHIPPER_ADDR1=JAKARTA%20NO%2044&OLSHOP_SHIPPER_ADDR2=KALIBATA&OLSHOP_SHIPPER_ADDR3=KALIBATA&OLSHOP_SHIPPER_CITY=JAKARTA&OLSHOP_SHIPPER_REGION=JAKARTA&OLSHOP_SHIPPER_ZIP=12345&OLSHOP_SHIPPER_PHONE=%2B6289876543212&OLSHOP_RECEIVER_NAME=ANA&OLSHOP_RECEIVER_ADDR1=BANDUNG%20NO%2012&OLSHOP_RECEIVER_ADDR2=CIBIRU&OLSHOP_RECEIVER_ADDR3=BANDUNG&OLSHOP_RECEIVER_CITY=BANDUNG&OLSHOP_RECEIVER_REGION=JAWA%20BARAT&OLSHOP_RECEIVER_ZIP=12365&OLSHOP_RECEIVER_PHONE=%2B6285789065432&OLSHOP_QTY=1&OLSHOP_WEIGHT='.$weight.'&OLSHOP_GOODSDESC=TEST&OLSHOP_GOODSVALUE=1000&OLSHOP_GOODSTYPE=1&OLSHOP_INST=TEST&OLSHOP_INS_FLAG=N&OLSHOP_ORIG=CGK10000&OLSHOP_DEST=BDO10000&OLSHOP_SERVICE='.$layanan.'&OLSHOP_COD_FLAG=N&OLSHOP_COD_AMOUNT=0',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded'
                ),
                ));

                $response = json_decode(curl_exec($curl), 1);

                curl_close($curl);
                return $response['detail'];
                break;
            default:
                # code...
                break;
        }
    }
}