<?php
class Rajaongkir
{
    public static function send($category,$value = 0)
    {
        switch ($category) {
            case 'provinsi':
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.rajaongkir.com/starter/province?key=177d36f35a3675d48c06226d2be83566',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                ));

                $response1 = json_decode(curl_exec($curl), 1);

                curl_close($curl);
                break;
            case 'kota':
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.rajaongkir.com/starter/city?province='.$value.'&key=177d36f35a3675d48c06226d2be83566',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                ));

                $response = json_decode(curl_exec($curl), 1);

                curl_close($curl);
                return $response['rajaongkir']['results'];
                break;

            case 'layanan':
                # code...
                break;
            
            default:
                # code...
                break;
        }
        return $response1['rajaongkir']['results'];
        // return $response['rajaongkir']['results'];

        // return $response;
    }
}
