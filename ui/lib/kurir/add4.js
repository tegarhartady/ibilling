$(document).ready(function(){
    $("#submit").click(function (e) {
        e.preventDefault();
        $('#ibox_form').block({ message:block_msg });
        var _url = $("#_url").val();
        $.post(_url + 'contacts/add-post/', $( "#rform" ).serialize())
            .done(function (data) {
                var sbutton = $("#submit");
                var _url = $("#_url").val();
                if ($.isNumeric(data)) {

                    window.location = _url + 'contacts/view/' + data;
                }
                else {
                    $('#ibox_form').unblock();
                    var body = $("html, body");
                    body.animate({scrollTop:0}, '1000', 'swing');
                    $("#emsgbody").html(data);
                    $("#emsg").show("slow");
                }
            });
    });
})

document.querySelector('#kurir4').addEventListener('click',function (e) {
    let member = document.querySelector('#member');
    localStorage.setItem('member2',member.value);

    let email = document.querySelector('#email');
    localStorage.setItem('email2',email.value);

    let nohp = document.querySelector('#nohp');
    localStorage.setItem('nohp2',nohp.value);

    let alamat = document.querySelector('#alamat');
    localStorage.setItem('alamat2',alamat.value);

    let state = document.querySelector('#state');
    localStorage.setItem('state2',state.value);

    let city = document.querySelector('#city');
    localStorage.setItem('city2',city.value);

    let district = document.querySelector('#district');
    localStorage.setItem('district2',district.value);

    let village = document.querySelector('#village');
    localStorage.setItem('village2',village.value);
})