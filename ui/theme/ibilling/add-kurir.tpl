{extends file="layouts/admin.tpl"}

{block name="content"}
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Detail Barang</h5>
                    </div>
                    <div class="ibox-content" id="rform">
                        <div class="alert" id="emsg">
                            <span id="emsgbody"></span>
                        </div>
                        <form id="kurir_form">
                            <div class="form-group col-xsx-12">
                                <div class="form-group col-md-6">
                                    <label for="pengirim">Pengirim</label>
                                    <select class="form-control" id="pengirim" name="pengirim">
                                        <option disabled selected>Pilih</option>
                                        {foreach $pe as $provinsi}
                                            <option value="{$provinsi['KAB_KOTA']}">{$provinsi['KAB_KOTA']}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="penerima">Penerima</label>
                                    <select class="form-control" id="penerima" name="penerima">
                                        <option disabled selected>Pilih</option>
                                        {foreach $pe as $provinsi}
                                            <option value="{$provinsi['KAB_KOTA']}">{$provinsi['KAB_KOTA']}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="specintruction">Spec Intruction</label>
                                <textarea class="form-control" id="specintruction" name="specintruction" placeholder="Spesifikasi" required> </textarea>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="weight">Weight</label>
                                    <input type="number" class="form-control" id="weight" name="weight">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="vweight">V - Weight</label>
                                    <input type="number" class="form-control" id="vweight" name="vweight" required>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="content">Content</label>
                                <textarea type="text" class="form-control" id="content" name="content" required></textarea>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="pieces">Pieces</label>
                                    <input type="number" class="form-control" id="pieces" name="pieces" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="insurance">Insurance</label>
                                    <input type="number" class="form-control" id="insurance" name="insurance" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="transport">Transport</label>
                                    <select id="transport" name="transport" class="form-control" required>
                                        <option disabled selected>Jenis</option>
                                        <option value="test1">Test1</option>
                                        <option value="test1">Test2</option>
                                    </select>
                                </div>
                            </div>
                            {* <a href="{$_url}kurir/add4" class="btn btn-danger">Back</a> *}
                            <a href="{$_url}kurir/add2" id="kurir1" type="submit" class="btn btn-primary">Next</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}