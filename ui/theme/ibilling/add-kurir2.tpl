{* {extends file="layouts/admin.tpl"} *}

{block name="content"}
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Layanan</h5>
                    </div>
                    <div class="ibox-content" id="ibox_form">
                        <form id="kurir_form2">
                            <div class="form-row col-xs-12">
                                <div class="col-md-4">
                                    <h3>JNE</h3>
                                    {foreach $pos as $a}
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios" value="{$a['service_display']}">
                                            <label class="form-check-label" for="exampleRadios1">
                                                {$a['service_display']} ({$a['currency']}. {$a['price']} - {$a['etd_from']}/{$a['etd_thru']} Hari)
                                            </label>
                                        </div>
                                    {/foreach}
                                </div>
                                {* <div class="col-md-5">
                                    <h3>SiCepat</h3>
                                    {foreach $cepat as $b}
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios" value="{$b['service']}">
                                            <label class="form-check-label" for="exampleRadios1"> 
                                                {$b['service']} - {$b['description']} ({$b['tariff']} - {$b['etd']})
                                            </label>
                                        </div>
                                    {/foreach}
                                </div> *}
                            </div>
                            <a href="{$_url}kurir/add" class="btn btn-danger">Back</a>
                            <a onclick="myFunction()" id="kurir2" class="btn btn-primary">Next</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}