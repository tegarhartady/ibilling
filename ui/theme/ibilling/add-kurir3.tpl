{extends file="layouts/admin.tpl"}

{block name="content"}
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Penerima</h5>
                    </div>
                    <div class="ibox-content" id="ibox_form">
                        <form id="kurir_form2">
                            <div class="form-row">
                                
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="nama">Nama</label>
                                    <select class="form-control" id="member" name="member">
                                        <option disabled selected>Pilih Member</option>
                                        {foreach $d as $member}
                                            <option value="{$member['id']}">{$member['account']}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nohp">HP</label>
                                    <input class="form-control" type="number" id="nohp" name="nohp">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input class="form-control" type="email" id="email" name="email">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="alamat">Alamat</label>
                                    <textarea class="form-control" id="alamat" name="alamat"></textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    {* <label for="member">Pilih member</label> *}
                                    <label for="provinsi">Provinsi</label>
                                    <select id="state" name="state" class="form-control">
                                        <option disabled selected>-Pilih-</option>
                                        {foreach $on as $provinces}
                                            <option value="{$provinces['kode']}">{$provinces['nama']}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="kota">Kota</label>
                                    <select class="form-control" id="city" name="city">
                                        <option disabled selected>Pilih kota</option>
                                        {* <option disabled>No Data</option> *}
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="kecamatan">Kecamatan</label>
                                    <select class="form-control" id="district" name="district">
                                        <option disabled selected>Pilih Kecamatan</option>
                                        <option disabled>No Data</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="kodepos">Desa</label>
                                    <select class="form-control" id="village" name="village">
                                        <option disabled selected>Pilih Desa</option>
                                        <option disabled>No Data</option>
                                    </select>
                                </div>
                            </div>
                            <a href="{$_url}kurir/add2" id="kurir2" class="btn btn-danger">Back</a>
                            <a href="{$_url}kurir/add4" id="kurir3" class="btn btn-primary">Next</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}