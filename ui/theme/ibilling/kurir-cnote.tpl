{extends file="layouts/admin.tpl"}

{block name="content"}
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                {* test {$cnote['cnote_no']} *}
                {* {$pe['kode']}{$pe['ExtractString']} *}
                
                {foreach $d as $cnote}
                    {if $cnote['status'] == 'sukses'}
                        <h1> {$cnote['status']}</h1>
                        <br>
                        <p> Nomor AWB : {$cnote['cnote_no']}</p>    
                    {/if}
                    {if $cnote['status'] == 'Error'}
                        <h1> {$cnote['cnote_no']}</h1>
                        <br>
                        <p> {$cnote['reason']}</p>                        
                    {/if}                    
                {/foreach}
                {foreach $pe as $pengirim}
                    <h1>{$pengirim['ExtractString']}</h1>
                {/foreach}
                </div>
            </div>
        </div>
    </div>
{/block}