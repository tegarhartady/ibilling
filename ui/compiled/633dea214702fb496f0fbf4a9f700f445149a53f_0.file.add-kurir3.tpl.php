<?php
/* Smarty version 3.1.33, created on 2021-04-24 21:17:29
  from 'F:\laragon\www\ibilling\ui\theme\ibilling\add-kurir3.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_608428792d7990_53251369',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '633dea214702fb496f0fbf4a9f700f445149a53f' => 
    array (
      0 => 'F:\\laragon\\www\\ibilling\\ui\\theme\\ibilling\\add-kurir3.tpl',
      1 => 1619273848,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_608428792d7990_53251369 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_914434932608428792bc022_07761542', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "layouts/admin.tpl");
}
/* {block "content"} */
class Block_914434932608428792bc022_07761542 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_914434932608428792bc022_07761542',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Penerima</h5>
                    </div>
                    <div class="ibox-content" id="ibox_form">
                        <form id="kurir_form2">
                            <div class="form-row">
                                
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="nama">Nama</label>
                                    <select class="form-control" id="member" name="member">
                                        <option disabled selected>Pilih Member</option>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['d']->value, 'member');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['member']->value) {
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['member']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['member']->value['account'];?>
</option>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nohp">HP</label>
                                    <input class="form-control" type="number" id="nohp" name="nohp">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input class="form-control" type="email" id="email" name="email">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="alamat">Alamat</label>
                                    <textarea class="form-control" id="alamat" name="alamat"></textarea>
                                </div>
                                <div class="form-group col-md-6">
                                                                        <label for="provinsi">Provinsi</label>
                                    <select id="state" name="state" class="form-control">
                                        <option disabled selected>-Pilih-</option>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['on']->value, 'provinces');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['provinces']->value) {
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['provinces']->value['kode'];?>
"><?php echo $_smarty_tpl->tpl_vars['provinces']->value['nama'];?>
</option>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="kota">Kota</label>
                                    <select class="form-control" id="city" name="city">
                                        <option disabled selected>Pilih kota</option>
                                                                            </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="kecamatan">Kecamatan</label>
                                    <select class="form-control" id="district" name="district">
                                        <option disabled selected>Pilih Kecamatan</option>
                                        <option disabled>No Data</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="kodepos">Desa</label>
                                    <select class="form-control" id="village" name="village">
                                        <option disabled selected>Pilih Desa</option>
                                        <option disabled>No Data</option>
                                    </select>
                                </div>
                            </div>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
kurir/add2" id="kurir2" class="btn btn-danger">Back</a>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
kurir/add4" id="kurir3" class="btn btn-primary">Next</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
}
/* {/block "content"} */
}
