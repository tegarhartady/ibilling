<?php
/* Smarty version 3.1.33, created on 2021-04-28 00:25:40
  from 'F:\laragon\www\ibilling\ui\theme\ibilling\kurir-cnote.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60884914247d65_08179703',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '46f1f951baed679b2836faead64ebac83e264ffe' => 
    array (
      0 => 'F:\\laragon\\www\\ibilling\\ui\\theme\\ibilling\\kurir-cnote.tpl',
      1 => 1619544338,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60884914247d65_08179703 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_82830518160884914228f52_05262457', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "layouts/admin.tpl");
}
/* {block "content"} */
class Block_82830518160884914228f52_05262457 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_82830518160884914228f52_05262457',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                                                
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['d']->value, 'cnote');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cnote']->value) {
?>
                    <?php if ($_smarty_tpl->tpl_vars['cnote']->value['status'] == 'sukses') {?>
                        <h1> <?php echo $_smarty_tpl->tpl_vars['cnote']->value['status'];?>
</h1>
                        <br>
                        <p> Nomor AWB : <?php echo $_smarty_tpl->tpl_vars['cnote']->value['cnote_no'];?>
</p>    
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['cnote']->value['status'] == 'Error') {?>
                        <h1> <?php echo $_smarty_tpl->tpl_vars['cnote']->value['cnote_no'];?>
</h1>
                        <br>
                        <p> <?php echo $_smarty_tpl->tpl_vars['cnote']->value['reason'];?>
</p>                        
                    <?php }?>                    
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pe']->value, 'pengirim');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['pengirim']->value) {
?>
                    <h1><?php echo $_smarty_tpl->tpl_vars['pengirim']->value['ExtractString'];?>
</h1>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
    </div>
<?php
}
}
/* {/block "content"} */
}
