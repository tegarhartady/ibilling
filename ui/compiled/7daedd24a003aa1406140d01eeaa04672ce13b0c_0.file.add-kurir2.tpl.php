<?php
/* Smarty version 3.1.33, created on 2021-05-09 22:24:18
  from 'F:\laragon\www\ibilling\ui\theme\ibilling\add-kurir2.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6097fea2730797_96797717',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7daedd24a003aa1406140d01eeaa04672ce13b0c' => 
    array (
      0 => 'F:\\laragon\\www\\ibilling\\ui\\theme\\ibilling\\add-kurir2.tpl',
      1 => 1620573853,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6097fea2730797_96797717 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5949313636097fea25056b6_12023258', "content");
}
/* {block "content"} */
class Block_5949313636097fea25056b6_12023258 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_5949313636097fea25056b6_12023258',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Layanan</h5>
                    </div>
                    <div class="ibox-content" id="ibox_form">
                        <form id="kurir_form2">
                            <div class="form-row col-xs-12">
                                <div class="col-md-4">
                                    <h3>JNE</h3>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pos']->value, 'a');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['a']->value) {
?>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios" value="<?php echo $_smarty_tpl->tpl_vars['a']->value['service_display'];?>
">
                                            <label class="form-check-label" for="exampleRadios1">
                                                <?php echo $_smarty_tpl->tpl_vars['a']->value['service_display'];?>
 (<?php echo $_smarty_tpl->tpl_vars['a']->value['currency'];?>
. <?php echo $_smarty_tpl->tpl_vars['a']->value['price'];?>
 - <?php echo $_smarty_tpl->tpl_vars['a']->value['etd_from'];?>
/<?php echo $_smarty_tpl->tpl_vars['a']->value['etd_thru'];?>
 Hari)
                                            </label>
                                        </div>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </div>
                                                            </div>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
kurir/add" class="btn btn-danger">Back</a>
                            <a onclick="myFunction()" id="kurir2" class="btn btn-primary">Next</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
}
/* {/block "content"} */
}
