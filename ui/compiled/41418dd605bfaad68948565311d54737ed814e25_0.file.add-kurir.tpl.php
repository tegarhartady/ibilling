<?php
/* Smarty version 3.1.33, created on 2021-05-09 23:00:22
  from 'F:\laragon\ibilling\ui\theme\ibilling\add-kurir.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60980716d34f48_79299880',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '41418dd605bfaad68948565311d54737ed814e25' => 
    array (
      0 => 'F:\\laragon\\ibilling\\ui\\theme\\ibilling\\add-kurir.tpl',
      1 => 1619880737,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60980716d34f48_79299880 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_119691581860980716d198a8_52512006', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "layouts/admin.tpl");
}
/* {block "content"} */
class Block_119691581860980716d198a8_52512006 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_119691581860980716d198a8_52512006',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Detail Barang</h5>
                    </div>
                    <div class="ibox-content" id="rform">
                        <div class="alert" id="emsg">
                            <span id="emsgbody"></span>
                        </div>
                        <form id="kurir_form">
                            <div class="form-group col-xsx-12">
                                <div class="form-group col-md-6">
                                    <label for="pengirim">Pengirim</label>
                                    <select class="form-control" id="pengirim" name="pengirim">
                                        <option disabled selected>Pilih</option>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pe']->value, 'provinsi');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['provinsi']->value) {
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['provinsi']->value['KAB_KOTA'];?>
"><?php echo $_smarty_tpl->tpl_vars['provinsi']->value['KAB_KOTA'];?>
</option>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="penerima">Penerima</label>
                                    <select class="form-control" id="penerima" name="penerima">
                                        <option disabled selected>Pilih</option>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pe']->value, 'provinsi');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['provinsi']->value) {
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['provinsi']->value['KAB_KOTA'];?>
"><?php echo $_smarty_tpl->tpl_vars['provinsi']->value['KAB_KOTA'];?>
</option>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="specintruction">Spec Intruction</label>
                                <textarea class="form-control" id="specintruction" name="specintruction" placeholder="Spesifikasi" required> </textarea>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="weight">Weight</label>
                                    <input type="number" class="form-control" id="weight" name="weight">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="vweight">V - Weight</label>
                                    <input type="number" class="form-control" id="vweight" name="vweight" required>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="content">Content</label>
                                <textarea type="text" class="form-control" id="content" name="content" required></textarea>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="pieces">Pieces</label>
                                    <input type="number" class="form-control" id="pieces" name="pieces" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="insurance">Insurance</label>
                                    <input type="number" class="form-control" id="insurance" name="insurance" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="transport">Transport</label>
                                    <select id="transport" name="transport" class="form-control" required>
                                        <option disabled selected>Jenis</option>
                                        <option value="test1">Test1</option>
                                        <option value="test1">Test2</option>
                                    </select>
                                </div>
                            </div>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
kurir/add2" id="kurir1" type="submit" class="btn btn-primary">Next</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
}
/* {/block "content"} */
}
