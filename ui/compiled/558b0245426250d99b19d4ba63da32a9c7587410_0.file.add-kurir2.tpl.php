<?php
/* Smarty version 3.1.33, created on 2021-05-09 23:00:26
  from 'F:\laragon\ibilling\ui\theme\ibilling\add-kurir2.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6098071a7baf13_76597729',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '558b0245426250d99b19d4ba63da32a9c7587410' => 
    array (
      0 => 'F:\\laragon\\ibilling\\ui\\theme\\ibilling\\add-kurir2.tpl',
      1 => 1620573853,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6098071a7baf13_76597729 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_808374266098071a79e338_54991398', "content");
}
/* {block "content"} */
class Block_808374266098071a79e338_54991398 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_808374266098071a79e338_54991398',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Layanan</h5>
                    </div>
                    <div class="ibox-content" id="ibox_form">
                        <form id="kurir_form2">
                            <div class="form-row col-xs-12">
                                <div class="col-md-4">
                                    <h3>JNE</h3>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pos']->value, 'a');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['a']->value) {
?>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios" value="<?php echo $_smarty_tpl->tpl_vars['a']->value['service_display'];?>
">
                                            <label class="form-check-label" for="exampleRadios1">
                                                <?php echo $_smarty_tpl->tpl_vars['a']->value['service_display'];?>
 (<?php echo $_smarty_tpl->tpl_vars['a']->value['currency'];?>
. <?php echo $_smarty_tpl->tpl_vars['a']->value['price'];?>
 - <?php echo $_smarty_tpl->tpl_vars['a']->value['etd_from'];?>
/<?php echo $_smarty_tpl->tpl_vars['a']->value['etd_thru'];?>
 Hari)
                                            </label>
                                        </div>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </div>
                                                            </div>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['_url']->value;?>
kurir/add" class="btn btn-danger">Back</a>
                            <a onclick="myFunction()" id="kurir2" class="btn btn-primary">Next</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
}
/* {/block "content"} */
}
